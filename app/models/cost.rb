class Cost < ActiveRecord::Base
  belongs_to :category

  validates :category, presence: true
  validates :amount, presence: true
  validates :amount, numericality: { only_integer: true }
end
